<?php

namespace Drupal\efap;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class ExtraFieldPluginManager.
 *
 * @package Drupal\efap
 */
class ExtraFieldPluginManager extends DefaultPluginManager {

  /**
   * ExtraFieldPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   Namespaces.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ExtraField', $namespaces, $module_handler, 'Drupal\efap\ExtraFieldInterface', 'Drupal\efap\ExtraField');
  }

  /**
   * Gets the Plugin instance.
   *
   * @param array $options
   *   Options to pass to the Plugin.
   *
   * @return false|object
   *   The Plugin object or FALSE.
   */
  public function getInstance(array $options) {
    $definition = $this->getDefinition($options['id']);
    $class = DefaultFactory::getPluginClass($options['id'], $definition);
    $field = new $class($options);
    return $field;
  }

}
