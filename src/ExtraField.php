<?php

namespace Drupal\efap;

use Drupal\Component\Annotation\Plugin;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an ExtraField annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\efap\ExtraFieldPluginManager
 * @see \Drupal\efap\ExtraFieldInterface
 *
 * @ingroup extra_field
 */
class ExtraField extends Plugin implements ExtraFieldInterface {

  /**
   * {@inheritdoc}
   */
  public function info() : array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $viewMode) : array {
    $output = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          Html::cleanCssIdentifier($this->definition['id']),
          'extra-field',
        ],
      ],
    ];
    return $output;
  }

}
