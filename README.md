CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Extra Fields As Plugin module exposes extra fields as a Plugin.

 * For a full description of the module visit:
   https://www.drupal.org/project/efap

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/efap


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Extra Fields As Plugin module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. In the src/Plugin folder of your module, create a folder called
       "ExtraField".
    3. Create new classes in this folder that extend from
       \Drupal\efap\ExtraField.


MAINTAINERS
-----------

 * Bart Vanhoutte - https://www.drupal.org/u/bart-vanhoutte

Supporting organization:

 * Duo - https://www.drupal.org/duo
